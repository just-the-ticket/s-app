package com.jo.application.data.entity;


import com.jo.application.annotations.DisplayName;
import com.jo.application.annotations.timeline.StartDate;
import com.jo.application.core.data.entity.ZJTEntity;
import com.vaadin.flow.router.PageTitle;
import jakarta.persistence.*;

import java.time.LocalDateTime;

@Entity
@Table(name = "zjt_driverassignment")
@PageTitle("Driver Assignment")
public class ZJTDriverAssignment implements ZJTEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "zjt_driverassignment_id")
    private int zjt_driverassignment_id;


    @ManyToOne
    @JoinColumn(name = "zjt_user_id")
    @DisplayName(value = "Driver")
    private ZJTUser driver;

    @ManyToOne
    @JoinColumn(name = "zjt_depot_id")
    @DisplayName(value = "Depot")
    private ZJTDepot depot;

    @ManyToOne
    @JoinColumn(name = "zjt_revenueclass_id")
    @DisplayName(value = "RevenueClass")
    private ZJTRevenueClass revenueClass;

    @Column(name = "startdate")
    @StartDate(className = "blue")
    @DisplayName(value = "Trip Start")
    private LocalDateTime startDate;

    @Column(name = "enddate")
    @DisplayName(value = "Trip End")
    private LocalDateTime endDate;

    @Column(name = "description")
    @DisplayName (value = "Description")
    private String description = "";

    //colorcode - background color of the content in timeline
    @Column(name = "colorcode")
    @DisplayName (value = "ColorCode")
    private String colorcode = "";

    @Column(name="isfinal")
    @DisplayName(value = "Final")
    private boolean isFinal;

    @Column(name="istripcomplete")
    @DisplayName(value = "Trip Complete")
    private boolean isTripComplete;

    //vehicle is in scheduled service - vehicle is automatically assigned
    @Column(name="isNotAvailable")
    @DisplayName(value = "Not Available")
    private boolean isNotAvailable;

    public int getId() {
        return zjt_driverassignment_id;
    }

    public ZJTUser getDriver() {
        return driver;
    }

    public void setDriver(ZJTUser driver) {
        this.driver = driver;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ZJTDepot getDepot() {
        return depot;
    }

    public void setDepot(ZJTDepot depot) {
        this.depot = depot;
    }

    public String getColorcode() {
        return colorcode;
    }

    public void setColorcode(String colorcode) {
        this.colorcode = colorcode;
    }

    public boolean isFinal() {
        return isFinal;
    }

    public void setFinal(boolean aFinal) {
        isFinal = aFinal;
    }

    public boolean isTripComplete() {
        return isTripComplete;
    }

    public void setTripComplete(boolean tripComplete) {
        isTripComplete = tripComplete;
    }

    public boolean isNotAvailable() {
        return isNotAvailable;
    }

    public void setNotAvailable(boolean notAvailable) {
        this.isNotAvailable = notAvailable;
    }

    public ZJTRevenueClass getRevenueClass() {
        return revenueClass;
    }

    public void setRevenueClass(ZJTRevenueClass revenueClass) {
        this.revenueClass = revenueClass;
    }

    //s_resourceassignment is confirmed
    @Column(name="isconfirmed")
    @DisplayName(value = "Confirmed")
    private boolean isConfirmed;

    //Time when the user updated  this - for ETL to pickup
    @Column(name="updated")
    @DisplayName(value = "Updated")
    private LocalDateTime updated;

    public boolean isConfirmed() {
        return isConfirmed;
    }

    public void setConfirmed(boolean confirmed) {
        isConfirmed = confirmed;
    }

    public LocalDateTime getUpdated() {
        return updated;
    }

    public void setUpdated(LocalDateTime updated) {
        this.updated = updated;
    }

    @ManyToOne
    @JoinColumn(name = "zjt_tripleg_id")
    @DisplayName(value = "Trip Leg")
    private ZJTTripLeg tripleg;


    public ZJTTripLeg getTripleg() {
        return tripleg;
    }

    public void setTripleg(ZJTTripLeg tripleg) {
        this.tripleg = tripleg;
    }

    /**
     * Distinguish border color by the assigned vehicle
     */
    @Column(name = "bordercolor")
    @DisplayName (value = "Border Color")
    private String bordercolor = "";

    public String getBordercolor() {
        return bordercolor;
    }

    public void setBordercolor(String bordercolor) {
        this.bordercolor = bordercolor;
    }


    //just the id for now - no need to link to entity
    @Column(name="zjt_trip_id")
    private String zjt_trip_id;

    public String getZjt_trip_id() {
        return zjt_trip_id;
    }

    public void setZjt_trip_id(String zjt_trip_id) {
        this.zjt_trip_id = zjt_trip_id;
    }

    @Column(name="updatedby")
    private String updatedby;

    public String getUpdatedby() {
        return updatedby;
    }

    public void setUpdatedby(String updatedby) {
        this.updatedby = updatedby;
    }
}
