package com.jo.application.data.entity;

import com.jo.application.core.data.entity.ZJTEntity;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.server.VaadinSession;
import jakarta.persistence.*;

import java.util.List;

@Entity
@Table(name = "zjt_depot_access")
@PageTitle("Depot Access")
public class ZJTDepotAccess implements ZJTEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "zjt_depot_access_id", nullable = false)
    private int id;

    @Column(name = "m_warehouse_id")
    private int warehouseId;

    @Column(name = "username", length = 100)
    private String username;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(int warehouseId) {
        this.warehouseId = warehouseId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public static int[] getAllowedDepots(EntityManager entityManager) {
        String user = (String) VaadinSession.getCurrent().getAttribute("user");
        String query = "SELECT e FROM " + ZJTDepotAccess.class.getSimpleName() + " e WHERE e.username = :username";
        TypedQuery<ZJTDepotAccess> typedQuery = entityManager.createQuery(query, ZJTDepotAccess.class);
        typedQuery.setParameter("username", user);
        List<ZJTDepotAccess> warehouseass = typedQuery.getResultList();
        // Create an array to hold warehouseIds
        int[] warehouseIds = new int[warehouseass.size()];
        // Iterate through the list and populate the array
        for (int i = 0; i < warehouseass.size(); i++) {
            warehouseIds[i] = warehouseass.get(i).getWarehouseId();
        }
        return warehouseIds;
    }
}