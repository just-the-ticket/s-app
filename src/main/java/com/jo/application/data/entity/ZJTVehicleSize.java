package com.jo.application.data.entity;

import com.jo.application.core.data.entity.ZJTEntity;
import com.vaadin.flow.router.PageTitle;
import jakarta.persistence.*;

@Entity
@Table(name = "zjt_vehiclesize")
@PageTitle("Vehicle Size")
public class ZJTVehicleSize implements ZJTEntity {


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "zjt_vehiclesize_id", nullable = false)
    private int zjt_vehiclesize_id;

    @Column(name = "name")
    private String name;

    public void setZjt_vehiclesize_id(int zjt_vehiclesize_id) {
        this.zjt_vehiclesize_id = zjt_vehiclesize_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int getId() {
        return zjt_vehiclesize_id;
    }
}
