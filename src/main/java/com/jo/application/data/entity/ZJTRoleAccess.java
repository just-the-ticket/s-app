package com.jo.application.data.entity;

import com.jo.application.core.data.entity.ZJTEntity;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.server.VaadinSession;
import jakarta.persistence.*;

import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "zjt_role_access")
@PageTitle("Role Access")
public class ZJTRoleAccess implements ZJTEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "zjt_role_access_id", nullable = false)
    private int id;

    @Column(name = "s_app_accesskey", length = 100)
    private String appAccessKey;

    @Column(name = "username", length = 100)
    private String username;

    // Getters and Setters
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAppAccessKey() {
        return appAccessKey;
    }

    public void setAppAccessKey(String appAccessKey) {
        this.appAccessKey = appAccessKey;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public static List<String> getAccessKeys(EntityManager entityManager) {
        String user = (String) VaadinSession.getCurrent().getAttribute("user");
        String query = "SELECT e FROM " + ZJTRoleAccess.class.getSimpleName() + " e WHERE e.username = :username";
        TypedQuery<ZJTRoleAccess> typedQuery = entityManager.createQuery(query, ZJTRoleAccess.class);
        typedQuery.setParameter("username", user);
        List<ZJTRoleAccess> resTypeSchedAccesses = typedQuery.getResultList();

        List<String> accessKeys = new ArrayList<String>();
        resTypeSchedAccesses.forEach(access -> accessKeys.add(access.appAccessKey));

        return accessKeys;
    }
}