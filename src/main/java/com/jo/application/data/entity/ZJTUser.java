package com.jo.application.data.entity;

import com.jo.application.annotations.BaseItems;
import com.jo.application.annotations.ContentDisplayedInSelect;
import com.jo.application.annotations.DisplayName;
import com.jo.application.core.data.entity.ZJTEntity;
import com.jo.application.data.repository.BaseEntityRepository;
import com.vaadin.flow.router.PageTitle;
import jakarta.persistence.*;
import org.hibernate.annotations.Immutable;
import org.hibernate.annotations.Subselect;

import java.util.List;


/**
 * Entity implementation class for Entity: ZJTUser
 */
@Entity
@Immutable
@Table(name = "`zjv_user`")
@Subselect("select * from zjv_user")
//@Table(name = "zjt_user")
@PageTitle("User")
public class ZJTUser implements ZJTEntity {


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "zjt_user_id")
    private int zjt_user_id;

    @Column
    @ContentDisplayedInSelect(value = "name")
    @DisplayName(value = "Name")
    private String name = "";

    @Column
    @DisplayName(value = "Search Key")
    private String value = "";

    @Column
    @DisplayName(value = "ClassName")
    private String classname;

    @ManyToOne
    @JoinColumn(name = "zjt_depot_id")
    @DisplayName(value = "Depot")
    private ZJTDepot depot;

    @Column(name = "isdriver")
    @DisplayName("Driver")
    private boolean isDriver;

    @Column(name = "islangspc")
    @DisplayName("Lang Spc")
    private boolean isLangSpc;

    @Column(name = "isguide")
    @DisplayName("Guide")
    private boolean isGuide;

    @Override
    public int getId() {
        return zjt_user_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public boolean isDriver() {
        return isDriver;
    }

    public void setDriver(boolean driver) {
        isDriver = driver;
    }

    public boolean isLangSpc() {
        return isLangSpc;
    }

    public void setLangSpc(boolean langSpc) {
        isLangSpc = langSpc;
    }

    public boolean isGuide() {
        return isGuide;
    }

    public void setGuide(boolean guide) {
        isGuide = guide;
    }

    public ZJTDepot getDepot() {
        return depot;
    }

    public void setDepot(ZJTDepot depot) {
        this.depot = depot;
    }

    public String getClassname() {
        return classname;
    }

    public void setClassname(String classname) {
        this.classname = classname;
    }

    static public ZJTUser getUser(String value, BaseEntityRepository repository)
    {
        List<ZJTUser> users = repository.getEntityManager()
                .createQuery("select  u from ZJTUser u where u.value = '" + value + "'")
                .getResultList();

        if (users.size() == 1) {
            return users.get(0);
        }
        return  null;

    }
}
