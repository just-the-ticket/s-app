package com.jo.application.data.entity;

import com.jo.application.annotations.BaseItems;
import com.jo.application.annotations.ContentDisplayedInSelect;
import com.jo.application.annotations.DisplayName;
import com.jo.application.core.data.entity.ZJTEntity;
import com.vaadin.flow.router.PageTitle;
import jakarta.persistence.*;
import org.hibernate.annotations.Immutable;
import org.hibernate.annotations.Subselect;

import java.util.List;

@Entity
//@Table(name = "zjt_vehicle")
@Immutable
@Table(name = "`zjv_vehicle`")
@Subselect("select * from zjv_vehicle")
@PageTitle("Vehicle")
public class ZJTVehicle implements ZJTEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "zjt_vehicle_id")
    private int zjt_vehicle_id;

    @Column
    @ContentDisplayedInSelect(value = "Fleet ID", sequence = 10)
//    @NotNull
    @DisplayName(value = "Fleet ID")
    private String fleetid;


    @Column
    @ContentDisplayedInSelect(value = "Name", sequence = 20)
    @DisplayName(value = "Name")
    private String name;

    @Column
    @DisplayName(value = "ClassName")
    private String classname;

    @ManyToOne
    @JoinColumn(name = "zjt_depot_id")
    @DisplayName(value = "Depot")
    private ZJTDepot depot;

    @ManyToOne
    @JoinColumn(name = "zjt_vehiclesize_id")
    @DisplayName(value = "Vehicle Size")
    private ZJTVehicleSize vehicleSize;

    public int getZjt_vehicle_id() {
        return zjt_vehicle_id;
    }


    @Override
    public int getId() {
        return zjt_vehicle_id;
    }

    public void setZjt_vehicle_id(int zjt_vehicle_id) {
        this.zjt_vehicle_id = zjt_vehicle_id;
    }

    public String getFleetid() {
        return fleetid;
    }

    public void setFleetid(String fleetid) {
        this.fleetid = fleetid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getClassname() {
        return classname;
    }

    public void setClassname(String classname) {
        this.classname = classname;
    }

    public ZJTDepot getDepot() {
        return depot;
    }

    public void setDepot(ZJTDepot depot) {
        this.depot = depot;
    }

    public ZJTVehicleSize getVehicleSize() {
        return vehicleSize;
    }

    public void setVehicleSize(ZJTVehicleSize vehicleSize) {
        this.vehicleSize = vehicleSize;
    }
}
