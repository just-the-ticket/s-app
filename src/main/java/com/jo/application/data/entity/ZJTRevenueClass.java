package com.jo.application.data.entity;

import com.jo.application.core.data.entity.ZJTEntity;
import com.vaadin.flow.router.PageTitle;
import jakarta.persistence.*;

@Entity
@Table(name = "zjt_revenueclass")
@PageTitle("Revenue Class")
public class ZJTRevenueClass implements ZJTEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "zjt_revenueclass_id", nullable = false)
    private int zjt_revenueclass_id;

    @Column(name = "name")
    private String name;

    public void setZjt_revenueclass_id(int zjt_revenueclass_id) {
        this.zjt_revenueclass_id = zjt_revenueclass_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int getId() {
        return zjt_revenueclass_id;
    }
}
