package com.jo.application.data.event;

import com.jo.application.data.entity.*;
import com.jo.application.data.repository.BaseEntityRepository;
import com.jo.application.events.ZJTContextFormEventHandler;
import com.jo.application.util.ZoomUtil;
import com.vaadin.componentfactory.timeline.util.TimelineUtil;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.jo.application.util.Util;

public class TripLegInfoFormEventHandler extends ZJTContextFormEventHandler {

    public enum AppView {
        VEHICLE,
        DRIVER,
        GUIDE,
        LANG_SPC
    }

    private AppView  appView = AppView.VEHICLE;
    private BaseEntityRepository repository = null;
    public TripLegInfoFormEventHandler(AppView appView, BaseEntityRepository repository, String content, String style) {
        super(content, style, true);
        this.appView = appView;
        this.repository = repository;
//        super("", "", false);
    }
    @Override
    public void onInputChange(String key, String value, int itemId) {

    }

    @Override
    public void onClickButton(String key, int itemId) {

    }

    @Override
    public String parsedContent(String content, int itemId) {
        String html = content;
        ZJTTripLeg tripLeg = null;
        switch ( this.appView) {
            case VEHICLE:
                ZJTVehicleAssignment va = (ZJTVehicleAssignment) this.repository.findEntityById(ZJTVehicleAssignment.class, itemId);
                html = Util.htmlParse(html, va, true);
                tripLeg = va.getTripleg();
                break;
            case DRIVER:
                ZJTDriverAssignment da = (ZJTDriverAssignment) this.repository.findEntityById(ZJTDriverAssignment.class, itemId);
                html = Util.htmlParse(html, da, true);
                tripLeg = da.getTripleg();
                break;
            case LANG_SPC:
                ZJTLangSpcAssignment la = (ZJTLangSpcAssignment) this.repository.findEntityById(ZJTLangSpcAssignment.class, itemId);
                html = Util.htmlParse(html, la, true);
                tripLeg = la.getTripleg();
                break;
            case GUIDE:
                ZJTGuideAssignment ga = (ZJTGuideAssignment) this.repository.findEntityById(ZJTGuideAssignment.class, itemId);
                html = Util.htmlParse(html, ga, true);
                tripLeg = ga.getTripleg();
                break;

        }

        String tripURL = ZoomUtil.generateURL(260, tripLeg.getId());
        html = html.replace("{tripURL}", tripURL);
        return Util.htmlParse(html, tripLeg, false);

    }

    @Override
    public void onSubmitForm(Map<String, Object> formDataMap, int itemId) {

    }


}
