package com.jo.application.data.event;

import com.vaadin.componentfactory.timeline.context.ItemContextMenuEventHandler;

public class TripLegInfoContextMenuEventHandler extends ItemContextMenuEventHandler {

    public TripLegInfoContextMenuEventHandler() {
        super();
    }

    @Override
    protected void loadGroupItemsContextMenu(int left, int top, int selectedGroupID) {
        super.loadGroupItemsContextMenu(left, top, selectedGroupID);
    }

    @Override
    protected void loadItemsContextMenu(int left, int top, int selectedItemID) {
        super.loadItemsContextMenu(left, top, selectedItemID);
    }

    @Override
    public void loadContextMenu(int left, int top, int selectedCompID, String componentType) {
        super.loadContextMenu(left, top, selectedCompID, componentType);
    }
}
