package com.jo.application.util;

import com.jo.application.core.form.TimeLineViewParameter;
import com.vaadin.componentfactory.timeline.util.TimelineUtil;
import jdk.dynalink.beans.StaticClass;
import org.apache.commons.lang3.StringUtils;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Util {

    /**
     * Based from TimelineUtil.parseString, but with finer control
     * @param content
     * @param context
     * @return
     */
    public static String htmlParse(String content, Object context, boolean keepVariable)
    {
        Pattern pattern = Pattern.compile("\\{([^}]+)}");
        Matcher matcher = pattern.matcher(content);
        StringBuilder result = new StringBuilder();

        while (matcher.find()) {
            String replacement;
            String expression = matcher.group(1);
            replacement = contextTranslate(expression, context, keepVariable);

            matcher.appendReplacement(result, replacement);
        }
        matcher.appendTail(result);
        return result.toString();
    }

    public static String contextTranslate(String expression, Object context, boolean keepVariable)
    {
        Object value = null;
        String result = null;
        ExpressionParser parser = new SpelExpressionParser();
        try {
            // Try to get the value from the context
            value = parser.parseExpression(expression).getValue(context);
        } catch (Exception e) {
            // Handle exceptions if expression parsing or evaluation fails
        }

        // Handle different types with custom formatting
        if (value instanceof LocalDateTime) {
            result = TimelineUtil.formatDatesTime((LocalDateTime) value);
        } else if (value instanceof LocalDate) {
            result = TimelineUtil.formatDates((LocalDate) value);
        } else if (value instanceof Double) {
            result = TimelineUtil.formatDouble((Double) value);
        } else if (value instanceof Boolean) {
            result = ((Boolean) value) ? "checked" : "";
        } else if (value instanceof String && StringUtils.isEmpty((String)value)) {
            result = "";
        } else {
            result = value != null ? value.toString()
                    : keepVariable ? "{" + expression + "}" : "";
        }

        return result;
    }
}
