package com.jo.application.views;

import com.jo.application.components.appnav.AppNav;
import com.jo.application.components.appnav.AppNavItem;
import com.jo.application.data.entity.ZJTDepotAccess;
import com.jo.application.data.entity.ZJTRoleAccess;
import com.jo.application.data.repository.ApplicationContextProvider;
import com.jo.application.data.repository.StandardFormRepository;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.VaadinSession;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.vaadin.lineawesome.LineAwesomeIcon;

import java.util.List;

/**
 * The main view is a top-level placeholder for other views.
 */
@PageTitle("Main")
@Route(value = "")
//@CssImport(value = "./styles/timeline-items-style.css")
public class MainLayout extends CoreMainLayout {

    public MainLayout() {
        super();
        Image logo = new Image("themes/ampere-v2/images/logo.png", "Logo");
        this.addToDrawer(logo);

        handleLoginUser();

        setNavigation(getNavigation(), "Scheduling");
    }

    List<String> roleAccess = null;

    @Override
    protected AppNav getNavigation() {

        roleAccess = (List<String>) VaadinSession.getCurrent().getAttribute("allowedRoles");
        nav = new AppNav();

        AppNavItem parent = null;

        parent = new AppNavItem("Scheduling");
        parent.setIcon(LineAwesomeIcon.CALENDAR_WEEK_SOLID.create());
        nav.addItem(parent);

//        parent.addItem(new AppNavItem("User", "user", LineAwesomeIcon.USER.create())
//                .withParameter("layout", this.getClass().getName()));
//
//        parent.addItem(new AppNavItem("Vehicle", "vehicle", LineAwesomeIcon.BUS_SOLID.create())
//                .withParameter("layout", this.getClass().getName()));

        if (roleAccess.contains("s-vehicle")) {
            parent.addItem(new AppNavItem("Vehicle Assignment", "vehicle-assignment", LineAwesomeIcon.BUS_SOLID.create())
                    .withParameter("layout", this.getClass().getName()));
        }

        if (roleAccess.contains("s-driver")) {
            parent.addItem(new AppNavItem("Driver Assignment", "driver-assignment", LineAwesomeIcon.USER_COG_SOLID.create())
                    .withParameter("layout", this.getClass().getName()));
        }

        if (roleAccess.contains("s-lang")) {
            parent.addItem(new AppNavItem("Lang Spc Assignment", "langspc-assignment", LineAwesomeIcon.USER_ASTRONAUT_SOLID.create())
                    .withParameter("layout", this.getClass().getName()));
        }

        if (roleAccess.contains("s-guide")) {
            parent.addItem(new AppNavItem("Guide Assignment", "guide-assignment", LineAwesomeIcon.USER_CIRCLE.create())
                    .withParameter("layout", this.getClass().getName()));
        }

//        parent = new AppNavItem("Planning");
//        parent.setIcon(LineAwesomeIcon.CALENDAR_WEEK_SOLID.create());
//        nav.addItem(parent);
//        parent.addItem(new AppNavItem("Service Schedule Overview", "service-schedule-overview", LineAwesomeIcon.PRODUCT_HUNT.create())
//                .withParameter("layout", this.getClass().getName()));

        nav.setClassName("app-nav");
        return nav;
    }

    private void handleLoginUser()
    {
        if (VaadinSession.getCurrent().getAttribute("allowedRoles") != null) {
            return;
        }

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        String username = auth.getName();
        VaadinSession.getCurrent().setAttribute("user", username);
        if (ApplicationContextProvider.getApplicationContext() != null) {
            StandardFormRepository sfr = ApplicationContextProvider.getApplicationContext().getBean(StandardFormRepository.class);
            if (VaadinSession.getCurrent().getAttribute("allowedDepots") == null) {
                int[] warehouseIds = ZJTDepotAccess.getAllowedDepots(sfr.getEntityManager());
                VaadinSession.getCurrent().setAttribute("allowedDepots", warehouseIds);
            }
            if (VaadinSession.getCurrent().getAttribute("allowedRoles") == null) {
                List<String> accessKeys = ZJTRoleAccess.getAccessKeys(sfr.getEntityManager());
                VaadinSession.getCurrent().setAttribute("allowedRoles", accessKeys);
            }
        }

    }
}