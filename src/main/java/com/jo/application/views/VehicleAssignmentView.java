package com.jo.application.views;

import com.jo.application.core.data.entity.ZJTEntity;
import com.jo.application.core.form.EnumDateFilter;
import com.jo.application.core.form.TimeLineViewParameter;
import com.jo.application.data.entity.*;
import com.jo.application.data.event.TripLegInfoContextMenuEventHandler;
import com.jo.application.data.event.TripLegInfoFormEventHandler;
import com.jo.application.data.repository.BaseEntityRepository;
import com.jo.application.data.service.TableInfoService;
import com.vaadin.flow.component.combobox.MultiSelectComboBox;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.router.*;
import com.vaadin.flow.server.VaadinSession;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Route(value = "vehicle-assignment/:subcategory?/:filter?", layout = MainLayout.class)
public class VehicleAssignmentView  extends StandardFormView implements HasUrlParameter<String> {

    protected TimeLineViewParameter timeLineViewParameter;
    protected MultiSelectComboBox<ZJTDepot> depotComboBox = new MultiSelectComboBox<ZJTDepot>();

    protected MultiSelectComboBox<ZJTRevenueClass> revenueClassMultiSelCom = new MultiSelectComboBox<ZJTRevenueClass>();

    protected MultiSelectComboBox<ZJTVehicleSize> vehicleSizeMultiCombo = new MultiSelectComboBox<ZJTVehicleSize>();

    public VehicleAssignmentView(BaseEntityRepository repository, TableInfoService tableInfoService) {
        super(repository, tableInfoService);
        timeLineViewParameter = new TimeLineViewParameter(new String[]{"description"}
                , "vehicle"
                , new String[]{"startDate"}
                , new String[]{"endDate"}
                , "colorcode"
                , null);
        timeLineViewParameter.setStack(true);
        timeLineViewParameter.setIdFieldName("zjt_vehicleassignment_id");
        timeLineViewParameter.setFromDefinition("ZJTVehicleAssignment");
        timeLineViewParameter.setGroupClass(ZJTVehicle.class);
        timeLineViewParameter.setDateFilterOn("startDate");
        timeLineViewParameter.setSelectDefinition("fleetid");
        timeLineViewParameter.setGroupSelectDefinition("fleetid");
        timeLineViewParameter.setItemBorderClassField("bordercolor");
        timeLineViewParameter.setGroupWhereDefinitions(new String[]{"depot.zjt_depot_id", "p.zjt_vehicle_id < 10 OR p.vehicleSize.zjt_vehiclesize_id"});
        Object[] groupParam = {Arrays.asList(new Object[]{0})};
        timeLineViewParameter.setGroupParameters(groupParam);
        timeLineViewParameter.setWhereDefinitions(new String[]{"p.vehicle.zjt_vehicle_id > 10 OR depot.zjt_depot_id"
                , "p.vehicle.zjt_vehicle_id > 10 OR vehicleSize.zjt_vehiclesize_id"
                , "p.vehicle.zjt_vehicle_id > 10 OR p.revenueClass.zjt_revenueclass_id"
                , "isConfirmed", "isTripComplete"});
        Object[] itemParam = {Arrays.asList(new Object[]{0}), Arrays.asList(new Object[]{0}), true, false};
        timeLineViewParameter.setParameters(itemParam);
        timeLineViewParameter.setGroupCSSClass("classname");
        timeLineViewParameter.setShowItemSelector(true);  //to test this feature
        this.timeLineViewParameter.setGroupZoomTableID(539);  //a_asset
        ZJTTableInfo tableInfo = tableInfoService.findByTableName(this.timeLineViewParameter.getFromDefinition());

        TripLegInfoFormEventHandler eventHandler = new TripLegInfoFormEventHandler(
                TripLegInfoFormEventHandler.AppView.VEHICLE, this.repository
                , tableInfo.getHtmlContent(), tableInfo.getHtmlStyle());
        timeLineViewParameter.setContextFormEventHandler(eventHandler);
        timeLineViewParameter.setItemContextMenuEventHandler(new TripLegInfoContextMenuEventHandler());

        timeLineViewParameter.setSubgroupIDFieldName("zjt_trip_id");
        timeLineViewParameter.setStackSubgroups(true);
        super.setTimeLineViewParameter(timeLineViewParameter);
    }

    @Override
    public void setParameter(BeforeEvent event, @OptionalParameter String parameter) {
        if (parameter != null) {

        } else {

        }
    }

    @Override
    public void initLayout() {
        super.initLayout();
        int[] warehouseIds = (int[]) VaadinSession.getCurrent().getAttribute("allowedDepots");
        List<ZJTDepot> depots = (List<ZJTDepot>) this.repository.findEntitiesByIds(ZJTDepot.class, warehouseIds);

        depotComboBox.setWidth("300px");
        depotComboBox.setItems(depots);
        depotComboBox.setItemLabelGenerator(ZJTDepot::getName);

        depotComboBox.addValueChangeListener(e -> {
            try {
                filterTimeline();
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        });

        if (!depots.isEmpty()) {
            depotComboBox.setValue(depots.get(0));
        }
        this.addCustomComponent(0, depotComboBox);

        List<ZJTEntity> entities = this.repository.findAll(ZJTRevenueClass.class);
        List<ZJTRevenueClass> revenueClasses = entities.stream()
                .map(entity -> (ZJTRevenueClass) entity)
                .collect(Collectors.toList());

        revenueClassMultiSelCom.setWidth("300px");
        revenueClassMultiSelCom.setItems(revenueClasses);
        revenueClassMultiSelCom.setItemLabelGenerator(ZJTRevenueClass::getName);

        revenueClassMultiSelCom.addValueChangeListener(e -> {
            try {
                filterTimeline();
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        });

        if (!revenueClasses.isEmpty()) {
            revenueClassMultiSelCom.setValue(revenueClasses.get(0));
        }
        this.addCustomComponent(1, revenueClassMultiSelCom);

        //vehicle size
        List<ZJTEntity> entVS = this.repository.findAll(ZJTVehicleSize.class);
        List<ZJTVehicleSize> vehicleSizes = entVS.stream()
                .map(entity -> (ZJTVehicleSize) entity)
                .collect(Collectors.toList());

        vehicleSizeMultiCombo.setWidth("300px");
        vehicleSizeMultiCombo.setItems(vehicleSizes);
        vehicleSizeMultiCombo.setItemLabelGenerator(ZJTVehicleSize::getName);

        vehicleSizeMultiCombo.addValueChangeListener(e -> {
            try {
                filterTimeline();
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        });

        if (!vehicleSizes.isEmpty()) {
            vehicleSizeMultiCombo.setValue(vehicleSizes.get(0));
        }
        this.addCustomComponent(2, vehicleSizeMultiCombo);

        try {
            form.updateDateFilter(EnumDateFilter.TD);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        form.setDatePickerReadonly(true);
    }

    private void filterTimeline() throws Exception {
        List<Integer> depotID = Stream.of(0).collect(Collectors.toList());
        if (depotComboBox.getValue() != null) {
            depotID = (depotComboBox.getValue().stream().filter(Objects::nonNull).map(ZJTDepot::getId).collect(Collectors.toList()));
        }
        List<Integer> revenueClassID = Stream.of(0).collect(Collectors.toList());
        if (revenueClassMultiSelCom.getValue() != null) {
            revenueClassID = (revenueClassMultiSelCom.getValue().stream().filter(Objects::nonNull).map(ZJTRevenueClass::getId).collect(Collectors.toList()));
        }
        List<Integer> vSizeID = Stream.of(0).collect(Collectors.toList());
        if (vehicleSizeMultiCombo.getValue() != null) {
            vSizeID = (vehicleSizeMultiCombo.getValue().stream().filter(Objects::nonNull).map(ZJTVehicleSize::getId).collect(Collectors.toList()));
        }
        Object[] groupParam = {depotID, vSizeID};
        Object[] itemParam = {depotID, vSizeID, revenueClassID, true, false};
        timeLineViewParameter.setParameters(itemParam);
        timeLineViewParameter.setGroupParameters(groupParam);
        form.onUpdateForm();
    }

    @Override
    public void onTimelineItemUpdate(ZJTItem item, boolean isMultipleSelection) {
        //Update ResourceAssignment here
        //TODO findEntityById seems not working
        ZJTVehicleAssignment assignment = (ZJTVehicleAssignment) this.repository.findEntityById(ZJTVehicleAssignment.class, item.getId());
        ZJTVehicle vehicle = (ZJTVehicle) this.repository.findEntityById(ZJTVehicle.class, Integer.parseInt(item.getGroupId()));
        if (!isMultipleSelection) {
            assignment.setStartDate(item.getStartTime());
            assignment.setEndDate(item.getEndTime());
        }
//        List<ZJTVehicleAssignment> assignments = (List<ZJTVehicleAssignment>) this.repository.findEntitiesByIds(ZJTVehicleAssignment.class, new int[]{item.getId()});
//        List<ZJTVehicle> vehicles = (List<ZJTVehicle>) this.repository.findEntitiesByIds(ZJTVehicle.class, new int[]{ Integer.parseInt(item.getGroupId())});
//        ZJTVehicleAssignment assignment = assignments.get(0);
//        ZJTVehicle vehicle = vehicles.get(0);
        assignment.setVehicle(vehicle);
        assignment.setUpdated(LocalDateTime.now());
        String updatedby = (String) VaadinSession.getCurrent().getAttribute("user");
        if (updatedby != null) {
            assignment.setUpdatedby(updatedby);
        }
        this.repository.updateEntity(assignment);

        Notification.show(assignment.getDescription() + " has been assigned to " + vehicle.getFleetid());
    }
}
