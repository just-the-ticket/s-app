package com.jo.application.views;

import com.jo.application.core.data.entity.ZJTEntity;
import com.jo.application.core.form.EnumDateFilter;
import com.jo.application.core.form.TimeLineViewParameter;
import com.jo.application.data.entity.*;
import com.jo.application.data.event.TripLegInfoContextMenuEventHandler;
import com.jo.application.data.event.TripLegInfoFormEventHandler;
import com.jo.application.data.repository.BaseEntityRepository;
import com.jo.application.data.service.TableInfoService;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.combobox.MultiSelectComboBox;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.OptionalParameter;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.VaadinSession;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Route(value = "langspc-assignment/:subcategory?/:filter?", layout = MainLayout.class)
public class LangSpcAssignmentView extends StandardFormView implements HasUrlParameter<String> {

    protected TimeLineViewParameter timeLineViewParameter;
    protected MultiSelectComboBox<ZJTDepot> depotComboBox = new MultiSelectComboBox<ZJTDepot>();

    protected MultiSelectComboBox<ZJTRevenueClass> revenueClassMultiSelCom = new MultiSelectComboBox<ZJTRevenueClass>();

    public LangSpcAssignmentView(BaseEntityRepository repository, TableInfoService tableInfoService) {
        super(repository, tableInfoService);
        timeLineViewParameter = new TimeLineViewParameter(new String[]{"description"}
                , "langspc"
                , new String[]{"startDate"}
                , new String[]{"endDate"}
                , "colorcode"
                , null);
        timeLineViewParameter.setStack(true);
        timeLineViewParameter.setIdFieldName("zjt_langspcassignment_id");
        timeLineViewParameter.setFromDefinition("ZJTLangSpcAssignment");
        timeLineViewParameter.setGroupClass(ZJTUser.class);
        timeLineViewParameter.setDateFilterOn("startDate");
        timeLineViewParameter.setSelectDefinition("value");
        timeLineViewParameter.setItemEditableField("isNotAvailable");
        timeLineViewParameter.setEditableFlagInverted(true);
        timeLineViewParameter.setItemBorderClassField("bordercolor");
        timeLineViewParameter.setGroupSelectDefinition("value");
        timeLineViewParameter.setGroupWhereDefinitions(new String[]{"depot.zjt_depot_id", "isLangSpc" });
        Object[] groupParam = {Arrays.asList(new Object[]{0}), true};
        timeLineViewParameter.setGroupParameters(groupParam);
        Object[] itemParam = {Arrays.asList(new Object[]{0}), Arrays.asList(new Object[]{0}), true, true, false};
        timeLineViewParameter.setWhereDefinitions(new String[]{"p.langspc.zjt_user_id > 10 OR depot.zjt_depot_id"
                , "p.langspc.zjt_user_id > 10 OR p.revenueClass.zjt_revenueclass_id"
                , "langspc.isLangSpc", "isConfirmed", "isTripComplete" });
        timeLineViewParameter.setParameters(itemParam);

        timeLineViewParameter.setGroupCSSClass("classname");
        timeLineViewParameter.setShowItemSelector(true);
        this.timeLineViewParameter.setGroupZoomTableID(114);  //ad_user
        ZJTTableInfo tableInfo = tableInfoService.findByTableName(this.timeLineViewParameter.getFromDefinition());

        TripLegInfoFormEventHandler eventHandler = new TripLegInfoFormEventHandler(
                TripLegInfoFormEventHandler.AppView.LANG_SPC, this.repository
                , tableInfo.getHtmlContent(), tableInfo.getHtmlStyle());
        timeLineViewParameter.setContextFormEventHandler(eventHandler);
        timeLineViewParameter.setItemContextMenuEventHandler(new TripLegInfoContextMenuEventHandler());


        timeLineViewParameter.setSubgroupIDFieldName("zjt_trip_id");
        timeLineViewParameter.setStackSubgroups(true);
        super.setTimeLineViewParameter(timeLineViewParameter);
    }


    @Override
    public void initLayout() {
        super.initLayout();
        int[] warehouseIds = (int[]) VaadinSession.getCurrent().getAttribute("allowedDepots");
        List<ZJTDepot> depots = (List<ZJTDepot>) this.repository.findEntitiesByIds(ZJTDepot.class, warehouseIds);

        depotComboBox.setWidth("300px");
        depotComboBox.setItems(depots);
        depotComboBox.setItemLabelGenerator(ZJTDepot::getName);

        depotComboBox.addValueChangeListener(e -> {
            try {
                filterTimeline();
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        });

        if (!depots.isEmpty()) {
            depotComboBox.setValue(depots.get(0));
        }
        this.addCustomComponent(0, depotComboBox);

        List<ZJTEntity> entities = this.repository.findAll(ZJTRevenueClass.class);
        List<ZJTRevenueClass> revenueClasses = entities.stream()
                .map(entity -> (ZJTRevenueClass) entity)
                .collect(Collectors.toList());

        revenueClassMultiSelCom.setWidth("300px");
        revenueClassMultiSelCom.setItems(revenueClasses);
        revenueClassMultiSelCom.setItemLabelGenerator(ZJTRevenueClass::getName);

        revenueClassMultiSelCom.addValueChangeListener(e -> {
            try {
                filterTimeline();
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        });

        if (!revenueClasses.isEmpty()) {
            revenueClassMultiSelCom.setValue(revenueClasses.get(0));
        }
        this.addCustomComponent(1, revenueClassMultiSelCom);
        try {
            form.updateDateFilter(EnumDateFilter.TD);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        form.setDatePickerReadonly(true);
    }


    @Override
    public void setParameter(BeforeEvent event, @OptionalParameter String parameter) {
        if (parameter != null) {

        } else {

        }
    }

    private void filterTimeline() throws Exception {
        List<Integer> depotID =  Stream.of(0).collect(Collectors.toList());
        if (depotComboBox.getValue() != null) {
            depotID = (depotComboBox.getValue().stream().filter(Objects::nonNull).map(ZJTDepot::getId).collect(Collectors.toList()));
        }
        List<Integer> revenueClassID = Stream.of(0).collect(Collectors.toList());
        if (revenueClassMultiSelCom.getValue() != null) {
            revenueClassID = (revenueClassMultiSelCom.getValue().stream().filter(Objects::nonNull).map(ZJTRevenueClass::getId).collect(Collectors.toList()));
        }
        System.out.println(depotID.toString());
        System.out.println(revenueClassID.toString());
        Object[] paramValue = {depotID, true};
        Object[] itemValue = {depotID, revenueClassID, true, true, false};
        timeLineViewParameter.setParameters(itemValue);
        timeLineViewParameter.setGroupParameters(paramValue);
        form.onUpdateForm();
    }

    @Override
    public void onTimelineItemUpdate(ZJTItem item, boolean isMultipleSelection) {
        //Update ResourceAssignment here
        //TODO findEntityById seems not working
        ZJTLangSpcAssignment langSpcAssignment = (ZJTLangSpcAssignment) this.repository.findEntityById(ZJTLangSpcAssignment.class, item.getId());
        ZJTUser langSpc = (ZJTUser) this.repository.findEntityById(ZJTUser.class, Integer.parseInt(item.getGroupId()));
        if (!isMultipleSelection) {
            langSpcAssignment.setStartDate(item.getStartTime());
            langSpcAssignment.setEndDate(item.getEndTime());
        }
        langSpcAssignment.setLangspc(langSpc);
        langSpcAssignment.setUpdated(LocalDateTime.now());
        String updatedby = (String) VaadinSession.getCurrent().getAttribute("user");
        if (updatedby != null) {
            langSpcAssignment.setUpdatedby(updatedby);
        }
        this.repository.updateEntity(langSpcAssignment);

        Notification.show(langSpcAssignment.getDescription() + " has been assigned to " + langSpc.getName());
    }
}
