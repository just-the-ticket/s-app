# s-app
## Vehicle Maintenance App


## Notes to build and run in docker
mvn clean install -Pproduction

docker build . -t s-app:latest

docker run --net=host -env JAVA_OPTS=-Dspring.datasource.url=jdbc:postgresql://localhost:5432/myapp-db?currentSchema=adempiere s-app:latest

