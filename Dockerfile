FROM openjdk:17-jdk-slim
COPY target/*.jar s-app.jar
EXPOSE 8088
ENTRYPOINT ["java", "-jar", "/s-app.jar"]